import logo from './logo.svg';
import './App.css';
import Project from './components/Route/Project';
import { createContext, useState } from 'react';


export let GlobalVariableContext = createContext();
function App() {
  let [token, setToken] = useState(localStorage.getItem("token"));
  let [pref, setPref] = useState(localStorage.getItem("pref"));
  return (
    <>
    <GlobalVariableContext.Provider value={{token: token, setToken: setToken, pref: pref, setPref: setPref}}>
      <Project></Project>
    </GlobalVariableContext.Provider>
    </>
  );
}

export default App;
