import React, { useContext, useState } from 'react'
import { Dropdown } from 'react-bootstrap'
import { GlobalVariableContext } from '../App'

const Sort = () => {
  return (
    <Dropdown>
        <Dropdown.Toggle variant="success" id="dropdown-basic" style={{marginBottom: "5px"}}>
           {global.label?global.label:"Sort"}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            <Dropdown.Item href="/hTLocations?type=sort&sort=closest" onClick={() => global.setLabel("Closest")}>Closest</Dropdown.Item>
            <Dropdown.Item href="/hTLocations?type=sort&sort=rating" onClick={() => global.setLabel("Rating")}>Rating</Dropdown.Item>
            <Dropdown.Item href="/hTLocations?type=sort&sort=recently-added" onClick={() => global.setLabel("Recently Added")}>Recently Added</Dropdown.Item>
        </Dropdown.Menu>
    </Dropdown>
  )
}

export default Sort