import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import SimilarLocations from "./SimilarLocations";
import { Accordion, Card, Col, Container, Image, ListGroup, Row, Stack } from "react-bootstrap";
import { MapContainer, Marker, Popup, TileLayer, useMap } from "react-leaflet";
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';
import ShowMap from "./ShowMap";
import "../styles/Attributes.css"
import "../styles/images.css"
import { ToastContainer } from "react-toastify";
import Rating from "./Rating";


const ShowSpecificHTLocation = () => {
  let [edit, setEdit] = useState([]);
  let navigate = useNavigate();
  let [wishList, setWishList] = useState([]);

  let params = useParams();
  let [hTLocation, setHTLocation] = useState({});
  let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
    iconSize: [25, 41]
  });
  L.Marker.prototype.options.icon = DefaultIcon;
  let isInWishList = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/wishlists/`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      // console.log(result.data.result);
      let hTLocId = result.data.result.map((item, i) => {
        return item.hTLocationId;
      });
      // console.log(hTLocId);
      setWishList(hTLocId);
    } catch (error) { }
  };
  let isCreated = async () => {
    try {
      let hTLocationsOfUser = await axios({
        url: `http://localhost:8000/hTLocations/user`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      hTLocationsOfUser = hTLocationsOfUser.data.result;
      hTLocationsOfUser = hTLocationsOfUser.map((item, i) => {
        return item._id;
      });
      setEdit(hTLocationsOfUser);
    } catch (error) {
      console.log(error);
    }
  };
  let getSpecificHTLocation = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/hTLocations/${params.hTLocId}`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      // console.log(result.data.result);
      setHTLocation(result.data.result);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getSpecificHTLocation();
    isCreated();
    isInWishList();
  }, []);
  console.log(hTLocation);
  return (
    <div style={{ backgroundColor: "lightblue" }}>
      <ToastContainer></ToastContainer>
      <Container>
        <Row>
          <Col>
            <img src="/img2.jpg" style={{ margin: "auto", display: "block" }}></img>
          </Col>
        </Row>
        <Row xs={2}>
          <Col sm={8}>
            <Container style={{ padding: "20px", marginTop: "20px" }}>
              <Card style={{ width: "auto", borderRadius: "20px" }}>
                <Card.Body>
                  <div style={{ display: "flex" }}>
                    <Card.Title as="h2" style={{ marginRight: "10px" }}>{hTLocation?.hTName}</Card.Title>
                    {wishList.includes(hTLocation._id) ? (
                      <button
                        style={{ backgroundColor: "#FF7F7F", margin: "5px 5px 5px", padding: "5px" }}
                        onClick={() => {
                          let removeFromWishList = async () => {
                            try {
                              let result = await axios({
                                url: `http://localhost:8000/wishLists/${hTLocation._id}`,
                                method: `delete`,
                                headers: {
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                },
                              });
                              console.log(result.data.message);
                              isInWishList();
                            } catch (error) {
                              console.log(error);
                            }
                          };
                          removeFromWishList();
                        }}
                      >
                        Remove from Wishlist
                      </button>
                    ) : (
                      <button
                        style={{ backgroundColor: "lightgreen", margin: "5px 5px 5px", padding: "5px" }}
                        onClick={() => {
                          let addToWishList = async () => {
                            try {
                              let result = await axios({
                                url: `http://localhost:8000/wishLists/${hTLocation._id}`,
                                method: `post`,
                                headers: {
                                  Authorization: `Bearer ${localStorage.getItem(
                                    "token"
                                  )}`,
                                },
                              });
                              console.log(result.data.message);
                              isInWishList();
                            } catch (error) {
                              console.log(error);
                            }
                          };
                          addToWishList();
                        }}
                      >
                        Add to Wishlist
                      </button>
                    )}
                    <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                      <div>Rating:</div>
                      <Rating hTLocId={params.hTLocId}></Rating>
                    </div>
                  </div>

                  <Card.Subtitle as="h5" className="mb-2 text-muted">{hTLocation?.location}</Card.Subtitle>
                  <Card.Text>
                    <div style={{ display: "flex", marginBottom: "10px" }}>
                      <div style={{ marginRight: "30px" }}><b>Difficulty:</b> {hTLocation?.difficulty}</div>
                      <div style={{ marginRight: "30px" }}>
                        <b>Length:</b> {hTLocation?.length?.value} {hTLocation?.length?.unit}
                      </div>
                      <div style={{ marginRight: "30px" }}>
                        <b>Elevation Gain:</b> {hTLocation?.elevationGain?.value}
                        {hTLocation?.elevationGain?.unit}
                      </div>
                      <div><b>Route Type:</b> {hTLocation?.routeType}</div>
                    </div>
                    <div style={{ display: "flex", marginBottom: "10px" }}>
                      <div style={{ marginRight: "30px" }}>
                        <div><b>Start Point</b></div>
                        <ul>
                          <li><b>latitude:</b> {hTLocation?.start?.latitude}</li>
                          <li><b>longitude:</b> {hTLocation?.start?.longitude}</li>
                        </ul>
                      </div>
                      <div style={{ marginRight: "30px" }}>
                        <div><b>End Point</b></div>
                        <ul>
                          <li><b>latitude:</b> {hTLocation.end?.latitude}</li>
                          <li><b>longitude:</b> {hTLocation.end?.longitude}</li>
                        </ul>
                      </div>
                    </div>
                    {hTLocation.description?                    <div style={{ marginBottom: "10px" }}>
                      <div><b>Description:</b></div>
                      <div>{hTLocation?.description}</div>

                    </div>:null}

                    <div><b>Attributes</b></div>
                    <Container>
                      {hTLocation?.attributes?.map((attribute, i) => {
                        return (
                          <button className="attribute" key={i} onClick={(e) => {
                            navigate(`/htLocations?type=filter&attributes=${attribute}`);
                          }}>
                            {attribute}
                          </button>
                        );
                      })}
                    </Container>
                  </Card.Text>
                  <div>
                    {edit.includes(hTLocation._id) ? (
                      <button
                        onClick={() => {
                          navigate(`/hTLocations/update/${params.hTLocId}`);
                        }}
                      >
                        Edit
                      </button>
                    ) : null}
                  </div>
                </Card.Body>
              </Card>
              <br></br>

            </Container>
          </Col>
          <Col sm={4}>

            <Stack gap={3}>

              {/* {hTLocation.start ? <ShowMap startPosition={[hTLocation.start?.latitude, hTLocation.start?.longitude]} endPosition={[hTLocation.end?.latitude, hTLocation.end?.longitude]}></ShowMap> : null} */}
              <SimilarLocations hTLocId={params.hTLocId} setHTLocation={setHTLocation}></SimilarLocations>
            </Stack>
          </Col>
        </Row>
      </Container>
      {/* <h1>Name: {hTLocation.hTName}</h1>
      <p>Location: {hTLocation.location}</p>
      <p>Difficult: {hTLocation.difficulty}</p>
      <p>Description: {hTLocation.description}</p>
      <p>
        Length: {hTLocation.length?.value}
        {hTLocation.length?.unit}
      </p>
      <p>
        Elevation Gain: {hTLocation.elevationGain?.value}
        {hTLocation.elevationGain?.unit}
      </p>
      <p>Route Type: {hTLocation.routeType}</p>
      <div>
        <p>Starting Point</p>
        <ul>
          <li>latitude: {hTLocation.start?.latitude}</li>
          <li>longitude: {hTLocation.start?.longitude}</li>
        </ul>
      </div>
      <div>
        <p>Ending Point</p>
        <ul>
          <li>latitude: {hTLocation.end?.latitude}</li>
          <li>longitude: {hTLocation.end?.longitude}</li>
        </ul>
      </div>
      <p>Attributes</p>
      {hTLocation.attributes?.map((attribute, i) => {
        return (
          <button style={{ marginInline: "5px", backgroundColor: "lightgreen" }} key={i} onClick={(e)=> {
            navigate(`/htLocations?type=filter&attributes=${attribute}`);
          }}>
            {attribute}
          </button>
        );
      })}
      <br></br>
      {edit.includes(hTLocation._id) ? (
        <button
          onClick={() => {
            navigate(`/hTLocations/update/${params.hTLocId}`);
          }}
        >
          Edit
        </button>
      ) : null}
      <SimilarLocations hTLocId = {params.hTLocId} setHTLocation = {setHTLocation}></SimilarLocations> */}
    </div>
  );
};

export default ShowSpecificHTLocation;

/* 
<Container>
  <Row xs={1}>
    <div>1</div>
  </Row>
  <Row xs={2}>
  <Col> Col 1</Col>
  <Col><Stack></Stack></Col>
  </Row>
</Container>
*/
