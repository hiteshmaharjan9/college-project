import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Container, Table } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import "../styles/button.css";


const MyLocations = () => {
    let [myLocations, setMyLocations] = useState([]);
    let navigate = useNavigate();
    let getMyLocation = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/hTLocations/user`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
            console.log(result);
            setMyLocations(result.data.result);
        }
        catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        getMyLocation();
    }, []);
    return (
        <div>
            <h2 style={{ textAlign: "center" }}>My Locations</h2>
            {myLocations.length ? <Container>
                <Table striped bordered hover variant="light">
                    <thead>
                        <th>S.N.</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Route Type</th>
                        <th>Difficulty</th>
                        <th>Length</th>
                        <th>Options</th>
                    </thead>
                    <tbody>
                        {myLocations.map((location, i) => {
                            return (
                                <tr key={i}>
                                    <td>{i + 1}</td>
                                    <td>{location?.hTName}</td>
                                    <td>{location?.location}</td>
                                    <td>{location?.routeType}</td>
                                    <td>{location?.difficulty}</td>
                                    <td>{location?.length?.value}km</td>
                                    <td>
                                        <button
                                            style={{ marginBottom: "5px", marginRight: "10px", padding: "5px", backgroundColor: "lightgray" }}
                                            className="updateButton"
                                            onClick={() => {
                                                navigate(`/hTLocations/update/${location._id}`);
                                            }}
                                        >
                                            Edit
                                        </button>
                                        <button
                                            style={{ marginBottom: "5px", marginRight: "10px", padding: "5px", backgroundColor: "lightgreen" }}
                                            className="detailButton"
                                            onClick={() => {
                                                navigate(`/hTLocations/${location._id}`);
                                            }}
                                        >
                                            Details
                                        </button>
                                        <button
                                            style={{ marginBottom: "5px", marginRight: "10px", padding: "5px", backgroundColor: "#FF7F7F" }}
                                            className="removeButton"
                                            onClick={() => {
                                                let deleteLocation = async () => {
                                                    try {
                                                        let result = await axios({
                                                            url: `http://localhost:8000/hTLocations/${location._id}`,
                                                            method: `delete`,
                                                            headers: {
                                                                Authorization: `Bearer ${localStorage.getItem("token")}`
                                                            }
                                                        });
                                                        console.log(result);
                                                    }
                                                    catch (error) {
                                                        console.log(error);
                                                    }
                                                }
                                                deleteLocation();
                                                window.location.reload();
                                            }}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </Container> : <p style={{ textAlign: "center" }}>You have not created any locations</p>}
        </div>
    )
}

export default MyLocations