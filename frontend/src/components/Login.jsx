import { Form, Formik } from "formik";
import React, { useContext } from "react";
import FormikInput from "./Formik/FormikInput";
import axios from "axios";
import { displayError } from "../utils/toast";
import { Link, useNavigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { GlobalVariableContext } from "../App";
import { Container } from "react-bootstrap";

const Login = () => {
  let global = useContext(GlobalVariableContext);
  let navigate = useNavigate();
  let initialValues = {
    email: "",
    password: "",
  };
  navigator.geolocation.getCurrentPosition((pos) => {
    let crd = pos.coords;
    localStorage.setItem("latitude", crd.latitude);
    localStorage.setItem("longitude", crd.longitude);
    console.log(crd);
  }, (error) => console.log(error));
  let pref = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/preferences`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      });
      console.log(result);
      localStorage.setItem("pref", "1");
      global.setPref("1");
    }
    catch (error)
    {
      console.log(error);
    }
  }
  let onSubmit = async (data) => {
    console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/users/login`,
        method: `post`,
        data: data,
      });
      // console.log(result.data);
      localStorage.setItem("name", result.data.result.firstName);
      localStorage.setItem("token", result.data.token);
      global.setToken(result.data.token);
      await pref();
      if (localStorage.getItem("pref"))
      {
        navigate("/");
      }
      else
        navigate("/preferences");
    } catch (error) {
      // console.log(error.response.data.message);
      displayError(error.response.data.message);
    }
  };
  return (
    <div style={{backgroundColor:`lightblue`}}>
      <ToastContainer></ToastContainer>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {(formik) => {
          return (
            <Container>
              <Form>
                <div className="col-md-4">
                  <FormikInput
                    className="form-control"
                    label="Email"
                    id="email"
                    type="email"
                    name="email"
                    required={true}
                    onChange={(e) => {
                      formik.setFieldValue("email", e.target.value);
                    }}
                  ></FormikInput>
                </div>

                <br></br>
                <div className="form-group col-md-4">
                  <FormikInput
                    className="form-control"
                    label="Password"
                    id="password"
                    type="password"
                    name="password"
                    required={true}
                    onChange={(e) => {
                      formik.setFieldValue("password", e.target.value);
                    }}
                  ></FormikInput>
                </div>
                <br></br>
                <button type="submit">Log in</button>
                
              </Form>
      <Link to="/login/forgot-password">Forgot Password?</Link>

            </Container>
          );
        }}
      </Formik>
      {/* <div style={{cursor:"pointer"}} onClick={() => {
        navigate(`/login/forgot-password`);

      }}>Forgot password?</div> */}
    </div>
  );
};

export default Login;
