import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { displayError, displaySuccess } from "../utils/toast";
import Rating from "./Rating";

const HTLocationComponent = ({ hTLocId, getWishlist , index}) => {
  let [hTLocation, setHTLocation] = useState({});
  let [edit, setEdit] = useState([]);
  let [wishList, setWishList] = useState([]);
  let navigate = useNavigate();
  let getHTLocation = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/hTLocations/${hTLocId}`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result);
      setHTLocation(result.data.result);
    } catch (error) {
      console.log(error);
    }
  };
  let isCreated = async () => {
    try {
      let hTLocationsOfUser = await axios({
        url: `http://localhost:8000/hTLocations/user`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      hTLocationsOfUser = hTLocationsOfUser.data.result;
      hTLocationsOfUser = hTLocationsOfUser.map((item, i) => {
        return item._id;
      });
      setEdit(hTLocationsOfUser);
    } catch (error) {
      console.log(error);
    }
  };
  let isInWishList = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/wishlists/`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      // console.log(result.data.result);
      let hTLocId = result.data.result.map((item, i) => {
        return item.hTLocationId;
      });
      // console.log(hTLocId);
      setWishList(hTLocId);
    } catch (error) { }
  };

  useEffect(() => {
    getHTLocation();
    isInWishList();
    isCreated();
  }, []);

  return (
    <>
      {wishList.includes(hTLocId) ? (
        <>
        <td>{index + 1}</td>
          <td>{hTLocation.hTName}</td>
          <td>{hTLocation.location}</td>
          <td>{hTLocation.difficulty}</td>
          <td>
            Length: {hTLocation.length?.value}
          </td>
          <td><Rating hTLocId={hTLocId}></Rating></td>
          <td>
            <button
            style={{ marginRight: "10px" }}
            onClick={() => {
              navigate(`/hTLocations/${hTLocId}`);
            }}
          >
            Details
          </button></td>

          {/* {edit.includes(hTLocId) ? (
            <button
              style={{ marginRight: "10px" }}
              onClick={() => {
                navigate(`/hTLocations/update/${hTLocation._id}`);
              }}
            >
              Edit
            </button>
          ) : null} */}
          <td>
          <button
            style={{ backgroundColor: "#FF7F7F", marginRight: "10px" }}
            onClick={() => {
              let removeFromWishList = async () => {
                try {
                  let result = await axios({
                    url: `http://localhost:8000/wishLists/${hTLocation._id}`,
                    method: `delete`,
                    headers: {
                      Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                  });
                  console.log(result.data.message);
                  displaySuccess(result.data.message);
                  // getWishlist();
                  isInWishList();
                } catch (error) {
                  console.log(error);
                  displayError(error.response.data.message);
                }
              };
              removeFromWishList();
            }}
          >
            Remove from Wishlist
          </button>
          </td>
        </>
      ) : null}
    </>
  );
};

export default HTLocationComponent;
