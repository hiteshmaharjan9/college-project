import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { displayError, displaySuccess } from "../utils/toast";
import { ToastContainer } from "react-toastify";
import "../styles/Heading.css";
import Rating from "./Rating";
import Filter from "./Filter";
import { Card, Row, Col, Container, Button, ListGroup } from "react-bootstrap";
import Sort from "./Sort";
import '../styles/button.css';



const ShowAllHTLocation = () => {
  let [wishList, setWishList] = useState([]);
  // let [rating, setRating] = useState(0);
  let [hTLocations, setHTLocations] = useState([]);
  let [edit, setEdit] = useState([]);
  let [rating, setRating] = useState(0);
  let navigate = useNavigate();
  let [query, setQuery] = useSearchParams();
  console.log(query.get("attributes"));
  console.log(query.get("type"));

  let getAllHTLocation = async () => {
    try {
      let url = `http://localhost:8000/hTLocations`;
      let type = "";
      let attributes = "";
      let routeType = "";
      let difficulty = "";
      if (query.get("type")) url += `?type=${query.get("type")}`;
      if (query.get("routeType")) url += `&routeType=${query.get("routeType")}`;
      if (query.get("difficulty"))
        url += `&difficulty=${query.get("difficulty")}`;
      if (query.get("attributes"))
        url += `&attributes=${query.get("attributes")}`;
      // console.log("type = > ", `${type}`);
      if (query.get("sort")) url +=`&sort=${query.get("sort")}`;
      if (query.get("sort")==`closest`) url += `&latitude=${localStorage.getItem("latitude")}&longitude=${localStorage.getItem("longitude")}`
      console.log("URL => ", url);

      let result = await axios({
        url: url,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result);
      setHTLocations(result.data.result);
      // console.log(result);
    } catch (error) {
      console.log(error);
    }
  };

  let isCreated = async () => {
    try {
      let hTLocationsOfUser = await axios({
        url: `http://localhost:8000/hTLocations/user`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      hTLocationsOfUser = hTLocationsOfUser.data.result;
      hTLocationsOfUser = hTLocationsOfUser.map((item, i) => {
        return item._id;
      });
      setEdit(hTLocationsOfUser);
    } catch (error) {
      console.log(error);
    }
  };
  let isInWishList = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/wishlists/`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      // console.log(result.data.result);
      let hTLocId = result.data.result.map((item, i) => {
        return item.hTLocationId;
      });
      // console.log(hTLocId);
      setWishList(hTLocId);
    } catch (error) { }
  };
  useEffect(() => {
    getAllHTLocation();
    isInWishList();
    isCreated();
  }, []);

  let canIedit = async (hTLocation) => {
    let token = localStorage.getItem("token");
    if (!token) setEdit(false);
    else {
      try {
        let result = await axios({
          url: `http://locahost:8000/hTLocations/${hTLocation._id}`,
          method: `patch`,
          data: {},
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        console.log(result);
        setEdit(true);
      } catch (error) {
        console.log(error);
        setEdit(false);
      }
    }
  };
  return (
    <div style={{backgroundColor:"whitesmoke"}}>
      <ToastContainer></ToastContainer>
      <Container style={{ padding: "20px" }}>
        <h1>All Locations</h1>
        <Filter></Filter>
        <Sort></Sort>
        <Row xs={1} md={5} className="g-4">
          {Array.from(hTLocations).map((item, idx) => (
            <Col key={idx}>
              <Card>
                <Card.Img src="/img2.jpg"></Card.Img>
                <Card.Body>

                  <Card.Title>{item.hTName}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">{item.location}</Card.Subtitle>
                  <ListGroup>
                    <ListGroup.Item>
                      Length: {item.length.value}{item.length.unit}
                    </ListGroup.Item>
                    <ListGroup.Item>
                      Elevation Gain: {item.elevationGain.value}{item.elevationGain.unit}
                    </ListGroup.Item>
                  </ListGroup>
                </Card.Body>
                <button
                  style={{ marginBottom:"5px"}}
                  onClick={() => {
                    navigate(`${item._id}`);
                  }}
                >
                  Details
                </button>
                {edit.includes(item._id) ? (
                  <button
                    style={{ marginBottom: "5px"}}
                    onClick={() => {
                      navigate(`update/${item._id}`);
                    }}
                  >
                    Edit
                  </button>
                ) : null}
                {wishList.includes(item._id) ? (
                  <button
                    style={{ backgroundColor: "#FF7F7F", marginBottom: "5px"}}
                    onClick={() => {
                      let removeFromWishList = async () => {
                        try {
                          let result = await axios({
                            url: `http://localhost:8000/wishLists/${item._id}`,
                            method: `delete`,
                            headers: {
                              Authorization: `Bearer ${localStorage.getItem(
                                "token"
                              )}`,
                            },
                          });
                          console.log(result.data.message);
                          displaySuccess(result.data.message);
                          isInWishList();
                        } catch (error) {
                          console.log(error);
                          displayError(error.response.data.message);
                        }
                      };
                      removeFromWishList();
                    }}
                  >
                    Remove from Wishlist
                  </button>
                ) : (
                  <button
                    style={{ backgroundColor: "lightgreen", marginBottom: "5px" }}
                    onClick={() => {
                      let addToWishList = async () => {
                        try {
                          let result = await axios({
                            url: `http://localhost:8000/wishLists/${item._id}`,
                            method: `post`,
                            headers: {
                              Authorization: `Bearer ${localStorage.getItem(
                                "token"
                              )}`,
                            },
                          });
                          console.log(result.data.message);
                          displaySuccess(result.data.message);
                          isInWishList();
                        } catch (error) {
                          console.log(error);
                          displayError(error.response.data.message);
                        }
                      };
                      addToWishList();
                    }}
                  >
                    Add to Wishlist
                  </button>
                )}
                <Row md="auto" style={{display: "flex", alignItems:"center", justifyContent: "center"}}>
                <Col><label htmlFor="rating">Rating: </label></Col>
                <Col><Rating hTLocId={item._id}></Rating></Col>
                </Row>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
      {/* {hTLocations.map((hTLocation, i) => {
        // canIedit(hTLocation);
        return (
          <div
            key={i}
            style={
              i % 2 === 0
                ? { backgroundColor: "lightblue" }
                : { backgroundColor: "lightgoldenrodyellow" }
            }
          >
            <h2>{hTLocation.hTName}</h2>
            <p>Location: {hTLocation.location}</p>
            <p>Difficult: {hTLocation.difficulty}</p>
            <p>
              Length: {hTLocation.length.value}
              {hTLocation.length.unit}
            </p>
            <button
              style={{ marginRight: "10px" }}
              onClick={() => {
                navigate(`${hTLocation._id}`);
              }}
            >
              Details
            </button>
            {edit.includes(hTLocation._id) ? (
              <button
                style={{ marginRight: "10px" }}
                onClick={() => {
                  navigate(`update/${hTLocation._id}`);
                }}
              >
                Edit
              </button>
            ) : null}
            {wishList.includes(hTLocation._id) ? (
              <button
                style={{ backgroundColor: "#FF7F7F", marginRight: "10px" }}
                onClick={() => {
                  let removeFromWishList = async () => {
                    try {
                      let result = await axios({
                        url: `http://localhost:8000/wishLists/${hTLocation._id}`,
                        method: `delete`,
                        headers: {
                          Authorization: `Bearer ${localStorage.getItem(
                            "token"
                          )}`,
                        },
                      });
                      console.log(result.data.message);
                      displaySuccess(result.data.message);
                      isInWishList();
                    } catch (error) {
                      console.log(error);
                      displayError(error.response.data.message);
                    }
                  };
                  removeFromWishList();
                }}
              >
                Remove from Wishlist
              </button>
            ) : (
              <button
                style={{ backgroundColor: "lightgreen", marginRight: "10px" }}
                onClick={() => {
                  let addToWishList = async () => {
                    try {
                      let result = await axios({
                        url: `http://localhost:8000/wishLists/${hTLocation._id}`,
                        method: `post`,
                        headers: {
                          Authorization: `Bearer ${localStorage.getItem(
                            "token"
                          )}`,
                        },
                      });
                      console.log(result.data.message);
                      displaySuccess(result.data.message);
                      isInWishList();
                    } catch (error) {
                      console.log(error);
                      displayError(error.response.data.message);
                    }
                  };
                  addToWishList();
                }}
              >
                Add to Wishlist
              </button>
            )}

            <Rating hTLocId={hTLocation._id}></Rating>
          </div>
        );
      })} */}
    </div>
  );
};

export default ShowAllHTLocation;
