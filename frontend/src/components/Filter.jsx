import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const Filter = () => {
  let [easy, setEasy] = useState(false);
  let [moderate, setModerate] = useState(false);
  let [hard, setHard] = useState(false);
  let [loop, setLoop] = useState(false);
  let [pToP, setPToP] = useState(false);
  let [outAndBack, setOutAndBack] = useState(false);
  let [attr, setAttr] = useState([]);
  let navigate = useNavigate();
  let [attributes, setAttributes] = useState([]);

  let getAttr = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/attributes`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      });
      // console.log(result);
      setAttributes(result.data.result);
    }
    catch (error) {
      console.log(error.message);
    }
  }
  console.log("easy", easy);
  console.log("moderate", moderate);
  console.log("hard", hard);
  let onSubmit = (e) => {
    e.preventDefault();
    let difficulty = [];
    let routeType = [];
    if (easy == true)
      difficulty.push("easy");
    if (moderate == true)
      difficulty.push("moderate");
    if (hard == true)
      difficulty.push("hard");
    if (loop == true)
      routeType.push("loop");
    if (pToP == true)
      routeType.push("Point to Point");
    if (outAndBack == true)
      routeType.push("Out and Back");
    if (difficulty.length === 0) difficulty = ""; else difficulty = difficulty.join(",");
    console.log(difficulty);
    if (routeType.length === 0) routeType = ""; else routeType = routeType.join(",");
    console.log(routeType);
    if (attr.length === 0) attr = ""; else attr = attr.join(",");
    navigate(`/hTLocations?type=filter&difficulty=${difficulty}&routeType=${routeType}&attributes=${attr}`);
    window.location.reload();
  };

  useEffect(() => {
    getAttr();
  }, []);
  return (
    <div style={{border: "1px solid black", marginBottom:"10px"}}>
      <Container fluid>
        <form onSubmit={onSubmit}>
          <Row>
            <Col>
              <div><b>Difficulty:</b></div>
              <input type="checkbox" value={easy} onChange={(e) => { setEasy(e.target.checked); console.log(e.target.checked) }} id="easy"></input>
              <label htmlFor="easy">Easy</label>
              <br></br>
              <input type="checkbox" value={moderate} onChange={(e) => { setModerate(e.target.checked) }} id="moderate"></input>
              <label htmlFor="moderate">Moderate</label>
              <br></br>
              <input type="checkbox" value={hard} onChange={(e) => { setHard(e.target.checked) }} id="hard"></input>
              <label htmlFor="hard">Hard</label>
            </Col>
            <Col>
              <div><b>Route Type:</b></div>
              <input type="checkbox" value={loop} onChange={(e) => { setLoop(e.target.checked) }} id="loop"></input>
              <label htmlFor="loop">Loop</label>
              <br></br>
              <input type="checkbox" value={pToP} onChange={(e) => { setPToP(e.target.checked); console.log(e.target.checked) }} id="pToP"></input>
              <label htmlFor="pToP">Point to Point</label>
              <br></br>
              <input type="checkbox" value={outAndBack} onChange={(e) => { setOutAndBack(e.target.checked) }} id="outAndBack"></input>
              <label htmlFor="outAndBack">Out and Back</label>

            </Col>
            <Col>
              <div><b>Attributes</b></div>
              <select name="attributes" value={attr} onChange={(e) => {
                let options = [...e.target.selectedOptions];
                let values = options.map(option => option.value);
                setAttr(values);
              }} multiple={true}>
                {
                  attributes.map((a, i) => {
                    return (
                      <option value={a} key={i}>{a}</option>
                    );
                  })
                }
              </select>
            </Col>
          </Row>
          <button type="submit" style={{marginBottom: "5px"}}>Filter</button>
        </form>
      </Container>
    </div>
  )
}

export default Filter