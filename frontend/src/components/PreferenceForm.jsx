import axios from "axios";
import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import FormikInput from "./Formik/FormikInput";
import { displayError } from "../utils/toast";
import { ToastContainer } from "react-toastify";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { Col, Container, Row } from "react-bootstrap";

const PreferenceForm = () => {
    let navigate = useNavigate();
    let [length, setLength] = useState(0);
    let [elevationGain, setElevationGain] = useState(0);
    let [difficulty, setDifficulty] = useState("");
    let [routeType, setRouteType] = useState("");
    let [attributes, setAttributes] = useState([]);
    let [attr, setAttr] = useState([]);
    let getAttributes = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/attributes`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
            result = result.data.result;
            result = result.map((item, i) => {
                return { value: item, label: item };
            })
            setAttributes(result);
            console.log(result);
        } catch (error) {
            console.log(error);
        }
    };
    let onSubmit = async (e) => {
        e.preventDefault();
        if (Number.parseFloat(length) <= 20)
            length = "lte20";
        else if (Number.parseFloat(length) <= 50)
            length = "lte50";
        else
            length = "gt50";

        if (Number.parseFloat(elevationGain) <= 500)
            elevationGain = "lte500";
        else if (Number.parseFloat(elevationGain) <= 2000)
            elevationGain = "lte2000";
        else
            elevationGain = "gt2000";
        console.log(elevationGain);
        let data = {
            length: length,
            elevationGain: elevationGain,
            attributes: attr,
            difficulty: difficulty,
            routeType: routeType
        };
        console.log(data);
        try {
            let result = await axios({
                url: `http://localhost:8000/preferences`,
                method: `post`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                },
                data: data
            });
            console.log(result);
            navigate("/");
        }
        catch (error){
            console.log(error);
        }
    }
    useEffect(() => {
        getAttributes();
    }, [])

    return (
        <>
            <Container>
                <h2>Preferences</h2>
                <form onSubmit={onSubmit}>
                    <Row>
                        <Col>
                            <label>Maximum Length (in km):</label>
                            <input type="number" value={length} onChange={(e) => {
                                setLength(e.target.value);
                            }}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <label>Maximum Elevation Gain (in m):</label>
                            <input type="number" value={elevationGain} onChange={(e) => { setElevationGain(e.target.value) }}></input>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div><b>Difficulty:</b></div>
                            <input type="radio" value="easy" checked={difficulty === "easy"} onChange={(e) => { setDifficulty(e.target.value); }} id="easy"></input>
                            <label htmlFor="easy">Easy</label>
                            <br></br>
                            <input type="radio" value="moderate" checked={difficulty === "moderate"} onChange={(e) => { setDifficulty(e.target.value) }} id="moderate"></input>
                            <label htmlFor="moderate">Moderate</label>
                            <br></br>
                            <input type="radio" value="hard" checked={difficulty === "hard"} onChange={(e) => { setDifficulty(e.target.value) }} id="hard"></input>
                            <label htmlFor="hard">Hard</label>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div><b>Route Type:</b></div>
                            <input type="radio" value="Loop" onChange={(e) => { setRouteType(e.target.value) }} id="loop" checked={routeType === "Loop"}></input>
                            <label htmlFor="loop">Loop</label>
                            <br></br>
                            <input type="radio" value="Point to Point" onChange={(e) => { setRouteType(e.target.value); }} id="pToP" checked={routeType === "Point to Point"}></input>
                            <label htmlFor="pToP">Point to Point</label>
                            <br></br>
                            <input type="radio" value="Out & Back" onChange={(e) => { setRouteType(e.target.value) }} id="outAndBack" checked={routeType === "Out & Back"}></input>
                            <label htmlFor="outAndBack">Out and Back</label>
                        </Col>
                    </Row>
                    <Row>

                        <Col>
                            <div><b>Attributes</b></div>
                            <select name="attributes" value={attr} onChange={(e) => {
                                let options = [...e.target.selectedOptions];
                                let values = options.map(option => option.value);
                                setAttr(values);
                            }} multiple={true}>
                                {
                                    attributes.map((a, i) => {
                                        return (
                                            <option value={a.value} key={i}>{a.label}</option>
                                        );
                                    })
                                }
                            </select>

                        </Col>
                    </Row>
                    <button type="submit">Submit</button>
                </form>

            </Container>
        </>
    );
}

export default PreferenceForm