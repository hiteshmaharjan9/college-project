import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { GlobalVariableContext } from "../App";

let Rating = ({ hTLocId }) => {
  let [rating, setRating] = useState(0);
  // let [disable, setDisable] = useState(false);
  let global = useContext(GlobalVariableContext);

  let giveRating = async (e) => {
    try {
      let method = "";
      console.log("e.target.value before request", e.target.value);
      if (e.target.value == 0) {
        method = `delete`;
      }
      else if (isNaN(rating)) {
        method = "post";
      } else {
        method = "patch";
      }

      let result = await axios({
        url: `http://localhost:8000/ratings/hTLocations/${hTLocId}`,
        method: method,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        data: {
          rating: e.target.value,
        },
      });
      console.log(method);
      console.log(result);
      console.log("e.target.value after request", e.target.value);
      if (method == 'delete')
        setRating(0);
      else
        setRating(e.target.value);
    } catch (error) {
      console.log(error);
    }
  };

  let getRating = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/ratings/hTLocations/${hTLocId}`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      // console.log(result.data.result.rating);
      // console.log(rating);
      // if (result.data.result.rating)
      // {
      //   setDisable(true);
      // }

      setRating(result.data.result.rating);
      // console.log(rating);
    } catch (error) {
      console.log(error.message);
    }
  };
  // console.log(rating)
  useEffect(() => {
    getRating();
  }, [rating]);

  return (
    <>
      {global.token ?
   
            <select
              name="rating"
              id="rating"
              value={rating}
              onChange={giveRating}
            // onChange={(e) => {setRating(e.target.value); console.log(e.target.value)}}
            // defaultValue={}
            >
              <option value={0}>Not Given</option>
              <option value={1}>1</option>
              <option value={2}>2</option>
              <option value={3}>3</option>
              <option value={4}>4</option>
              <option value={5}>5</option>
            </select>
          : null}


      {/* <p>{rating}</p> */}
    </>
  );
};

export default Rating;
