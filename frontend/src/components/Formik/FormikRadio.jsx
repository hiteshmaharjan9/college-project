import { Field } from "formik";
import React from "react";

const FormikRadio = ({ options, name, onChange, required,label,...props }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form, meta }) => {
            // console.log("field", field);
            // console.log("meta", meta);
            // console.log(options)
          return (
            <>
              <br></br>
              <span><b>{label}</b>{required?<span style={{color: "red"}}>*</span>:null}: </span>
              {options.map((option, i) => {
                return (
                  <div key={i}>
                    <label htmlFor={option.value}><b>{option.label}:</b>{"  "}</label>
                    <input
                    {...field}
                    {...props}
                      type="radio"
                      value={option.value}
                      id={option.value}
                      checked={meta.value === option.value}
                      onChange={
                        onChange?onChange:field.onChange
                    }
                    ></input>
                    {/* {meta.error && meta.touched?<div>{meta.error}</div>:null} */}
                  </div>
                );
              })}
            </>
          );
        }}
      </Field>
    </>
  );
};

export default FormikRadio;
