import axios from "axios";
import React, { useEffect, useState } from "react";
import HTLocationComponent from "./HTLocationComponent";
import { ToastContainer } from "react-toastify";
import { Container, Table } from "react-bootstrap";

const ShowWishlist = () => {
  console.log("first");
  let [wishlist, setWishlist] = useState([]);
  let getWishlist = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/wishlists`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result);
      setWishlist(result.data.result);
      // let _wishlist = result.data.result;
      // // console.log("wishlist", _wishlist);
      // _wishlist = _wishlist.map(async(fav, i) => {
      //     // let getLoc = async () => {
      //         try {
      //             let result = await axios({
      //                 url: `http://localhost:8000/hTLocations/${fav.hTLocationId}`,
      //                 method: `get`,
      //                 headers: {
      //                     Authorization: `Bearer ${localStorage.getItem("token")}`
      //                 }
      //             });
      //             // wishlist.push(result.data.result);
      //             setWishlist(result.data.result);
      //             // console.log(result.data.result);
      //             return result.data.result;
      //         } catch (error) {
      //             console.log(error);
      //         }
      //     // };
      //     // return await getLoc();

      // });
      // console.log("_wishlist", _wishlist);
      // setWishlist(_wishlist);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getWishlist();
  }, []);
  return (
    <>
      <ToastContainer></ToastContainer>
      <Container>
        <h1 style={{ textAlign: "center" }}>Wishlist</h1>
        <Container>
          {wishlist.length ? (
              <Table striped bordered hover>
                <tr>
                  <th>S.N.</th>
                  <th>Name</th>
                  <th>Location</th>
                  <th>Difficulty</th>
                  <th>Length (in km)</th>
                  <th>Rating</th>
                  <th>Details</th>
                  <th>Remove</th>
                </tr>
                {wishlist.map((fav, i) => {
                  console.log(i);
                  return (
                    <tr key={i} style={{ backgroundColor: "lightsteelblue" }}>
                      <HTLocationComponent
                        index={i}
                        hTLocId={fav?.hTLocationId}
                        getWishlist={getWishlist}
                      ></HTLocationComponent>
                    </tr>
                  );
                })}
              </Table>
          ) : <p style={{ textAlign: "center" }}>Wishlist is empty</p>}
        </Container>
      </Container>
    </>
  );
};

export default ShowWishlist;
