import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Rating from './Rating';
import { Container, Table } from 'react-bootstrap';
import ListRatingComp from './ListRatingComp';
import '../styles/button.css';
// import '../styles/bg.css';

const ListUserRating = () => {
    let [ratings, setRatings] = useState([]);
    let [hTLocations, setHTLocations] = useState([]);
    let getRating = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/ratings/users`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
            // console.log(result);
            setRatings(result.data.result);
        }
        catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getRating();
    }, [ratings]);
    return (
        <div className="bg" style={{backgroundColor:"whitesmoke"}}>
            <Container>
                <h2 style={{textAlign:"center"}}>My Ratings</h2>
                {ratings.length?                <Table striped border hover>
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Rating</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ratings.map((rating, i) => {
                            return (
                                <tr key={i}>
                                    <th>{i+1}</th>
                                    <ListRatingComp hTLocationId={rating?.hTLocationId}></ListRatingComp>
                                    <td><Rating hTLocId={rating?.hTLocationId}></Rating></td>
                                    
                                    <td><button className="removeButton" onClick={async () => {
                                        try {
                                            let result = await axios({
                                                url: `http://localhost:8000/ratings/hTLocations/${rating?.hTLocationId}`,
                                                method: `delete`,
                                                headers: {
                                                    Authorization: `Bearer ${localStorage.getItem("token")}`
                                                }
                                            });
                                            // console.log(result);
                                            window.location.reload();
                                        }
                                        catch (error) {
                                            console.log(error);
                                        }
                                    }}>remove</button></td>
                                </tr>
                            )
                        })}
                    </tbody>

                </Table>:<div style={{textAlign:"center"}}>No ratings available</div>}
            </Container>
        </div>
    )
}

export default ListUserRating