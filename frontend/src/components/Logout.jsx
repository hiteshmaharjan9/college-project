import React, { useContext, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { GlobalVariableContext } from '../App';

const Logout = () => {
    let global = useContext(GlobalVariableContext);
    let navigate = useNavigate();
    let deleteToken = () => {
        localStorage.removeItem("token");
        global.setToken(null);
        global.setPref(null);
        localStorage.removeItem("pref");
        navigate("/hTLocations");
    };
    useEffect(() => {
        deleteToken();
    }, [])
  return (
    <h1>Logging Out</h1>
  )
}

export default Logout