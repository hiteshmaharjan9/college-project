import axios from 'axios';
import React, { useEffect, useState } from 'react'

const ListRatingComp = ({hTLocationId}) => {
    let [hTLocation, setHTLocation] = useState([]);
    let getHTLocations = async () => {
        console.log("***");
        try {

            let result = await axios({
                url: `http://localhost:8000/hTLocations/${hTLocationId}`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
            // console.log(result);
            setHTLocation(result.data.result);

        }
        catch (error) {
            console.log(error);
        }
    };
    useEffect(()=> {
        getHTLocations();
    },[]);

    return (
        <>
            <td>{hTLocation?.hTName}</td>
            <td>{hTLocation?.location}</td>
        </>
    )
}

export default ListRatingComp