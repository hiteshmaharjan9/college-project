import React, { useContext, useEffect, useState } from 'react'
import { Link, NavLink } from 'react-router-dom'
import '../styles/NavBar.css'
import { Container, Nav, NavDropdown, Navbar } from "react-bootstrap";
import axios from "axios";
import { GlobalVariableContext } from '../App';

const NavBar = () => {
  let global = useContext(GlobalVariableContext);
  // let getName = async () => {
  //   try {
  //     let result = await axios({
  //       url: `http://localhost:8000/users/my-profile`,
  //       method: `get`,
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem("token")}`
  //       }
  //     });
  //     setName(name);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
  // useEffect(() => {
  //   getName();
  // });
  return (
    // <>
    //   <div id="NavBar">
    //     <NavLink to="/" style={{ marginRight: "20px" }} className="navItem">Home</NavLink>
    //     <NavLink to="/hTLocations" style={{ marginRight: "20px" }} className="navItem">All Locations</NavLink>
    //     <NavLink to="/hTLocations/create" style={{ marginRight: "20px" }} className="navItem">Create New Location</NavLink>

    //   <div class="dropdown">
    //     <button class="dropbtn">Profile</button>
    //     <div class="dropdown-content">



    //       <NavLink to="/users/my-profile" style={{ marginRight: "20px" }} className="navItem">My Profile</NavLink>
    //       <NavLink to="/users/wishlist" style={{ marginRight: "20px" }} className="navItem">Wishlist</NavLink>
    //     </div>
    //   </div>
    //   </div>
    // </>
    // <div id="NavBar">

    //   {/* <span onClick={() => {console.log("Hello")}} style={{cursor: "pointer"}}>Log Out</span>
    //    <NavLink ></NavLink> */}
    //   <form>
    //    <input type="search" placeholder="search"></input>
    //    <button class="searchButton">Search</button>
    //   </form>
    // </div>
    <>
        {/* <nav className="bg-gray-800 py-4">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex justify-between items-center">
          <div className="flex-shrink-0">
            <Link to="/" className="text-white font-bold text-xl">Your Logo</Link>
          </div>
          <div className="hidden md:block">
            <div className="ml-10 flex items-baseline space-x-4">
              <Link to="/" className="text-gray-300 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Home</Link>
              <Link to="/about" className="text-gray-300 hover:text-white px-3 py-2 rounded-md text-sm font-medium">About</Link>
              <Link to="/services" className="text-gray-300 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Services</Link>
              <Link to="/contact" className="text-gray-300 hover:text-white px-3 py-2 rounded-md text-sm font-medium">Contact</Link>
            </div>
          </div>
        </div>
      </div>
    </nav> */}
      <Navbar sticky="top" bg="primary" data-bs-theme="dark" expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand><NavLink to="/">HTLoc Recommender</NavLink></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              {global.token ? <Nav.Link><NavLink to="/hTLocations/create" className="navItem">Create New Location</NavLink></Nav.Link> : null}
              <Nav.Link><NavLink to="/hTLocations" className="navItem">All Locations</NavLink></Nav.Link>
              {!global.token ?
                <><Nav.Link>
                  <NavLink to="/login" className="navItem">Login</NavLink>
                </Nav.Link>
                  <Nav.Link>
                    <NavLink to="/register" className="navItem">Register</NavLink>
                  </Nav.Link></> : null}
            </Nav>
            {global.token ? <Nav>
              <NavDropdown title={`Welcome ${localStorage.getItem("name")}`} id="basic-nav-dropdown">
                <NavDropdown.Item><NavLink to="/users/my-profile" className="navItem">My Profile</NavLink></NavDropdown.Item>
                <NavDropdown.Item>
                  <NavLink to="/users/my-locations" className="navItem">My Locations</NavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <NavLink to="/users/ratings" className="navItem">My Ratings</NavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <NavLink to="/users/wishlist" className="navItem">Wishlist</NavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <NavLink to="/logout" className="navItem">Log Out</NavLink>
                </NavDropdown.Item>
              </NavDropdown>
            </Nav> : null}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  )
}

export default NavBar