import { MapContainer, Marker, Popup, TileLayer, useMap } from "react-leaflet";
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
    iconSize: [25,41]
});

L.Marker.prototype.options.icon = DefaultIcon;


let ShowMap = ({startPosition, endPosition}) => {
    // let position = [27.659295, 85.319838];
    return (
      <>
        <MapContainer
          center={startPosition}
          zoom={9}
          scrollWheelZoom={false}
          style={{
            height: "400px",
            backgroundColor: "red",
            marginTop: "80px",
            marginBottom: "90px",
            width: "400px"
          }}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={startPosition}>
            <Popup>
              Start Point<br></br>
              Latitude: {startPosition[0]}<br></br>
              Longitude: {startPosition[1]}<br></br>
            </Popup>
          </Marker>
          <Marker position={endPosition}>
          <Popup>
              End Point<br></br>
              Latitude: {endPosition[0]}<br></br>
              Longitude: {endPosition[1]}<br></br>
            </Popup>
          </Marker>

        </MapContainer>
      </>
    );
  };
  
  export default ShowMap;