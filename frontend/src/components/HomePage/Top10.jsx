
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Card, Row, Col, Container, Button, ListGroup } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

const Top10 = () => {
    let [list, setList] = useState([]);
    let navigate = useNavigate();
    const nOfItems = 10;
    let getTop10 = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/hTLocations?type=sort&sort=rating&page=${nOfItems}`,
                method: `get`
            });
            console.log(result);
            setList(result.data.result);
        }
        catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        getTop10();
    }, []);
    return (
        <div style={{backgroundColor:"#f7f4ed"}}>

            {/* {list.map((item, i) => {
                return (
                    <div key={i}>
                        <h3>{item.hTName}</h3>
                        <p>{item.location}</p>
                        <p>Average Rating: {item.avgRating}</p>
                        <p>Length: {item.length.value} {item.length.unit}</p>
                    </div>
                )
            })} */}
            <Container style={{padding: "20px"}}>
                <h2>Top 10 Highest Rated</h2>
                <Row xs={1} md={5} className="g-4">
                    {Array.from(list).map((item, idx) => (
                        <Col key={idx}>
                            <Card>
                                <Card.Img src="/img1.jpg"></Card.Img>
                                <Card.Body>

                                    <Card.Title>{item?.hTName}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{item?.location}</Card.Subtitle>
                                    <ListGroup>
                                        <ListGroup.Item>
                                            Length: {item?.length?.value}{item?.length?.unit}
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Elevation Gain: {item?.elevationGain?.value}{item?.elevationGain?.unit}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                                <Button onClick={() => { navigate(`/hTLocations/${item?._id}`) }}>View Details</Button>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>



        </div>
    )
}

export default Top10