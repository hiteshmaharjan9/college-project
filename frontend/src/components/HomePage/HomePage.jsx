import React, { useEffect } from 'react'
import Recommendation from './Recommendation'
import Closest from './Closest'
import Top10 from './Top10'
import { Carousel } from 'react-bootstrap'
import "./equalHeight.css";

const HomePage = () => {

  return (
    <div style={{backgroundColor: 'lightblue'}}>
      <Carousel>
        <Carousel.Item>
          <img src="./img1.jpg" className="carousel"/>
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img src="./img2.jpg"  className="carousel"/>

          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img src="./img3.jpg" className="carousel"/>

          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <Recommendation></Recommendation>
      <Closest></Closest>
      <Top10></Top10>
    </div>
  )
}

export default HomePage