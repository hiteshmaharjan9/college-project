import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Row, Col, Container, Button, ListGroup } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

const Recommendation = () => {
  let [x, setX] = useState(0);
  let [recHTL, setRecHTL] = useState([]);
  let navigate = useNavigate();
  let getRecommendation = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/hTLocations/recommendation`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(`token`)}`,
        },
      });
      // console.log(result);
      setRecHTL(result.data.result);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getRecommendation();
  }, []);
  // console.log(recHTL);

  return (
    <>
      <div style={{backgroundColor:"#f7f4ed"}}>

        {/* {recHTL.map((loc, i) => {
          return (
            <div>
              <img
                src="http://localhost:8000/images/image.jpg"
                alt="Hitesh Maharjan"
                height={300}
                width={600}
              ></img>
              <h3>{loc.hTName}</h3>
              <p>{loc.location}</p>
              <p>
                Length: {loc.length.value} {loc.length.unit}
              </p>
            </div>
          );
        })} */}
        <Container style={{padding: "20px"}}>
        <h2>Recommended Locations for you</h2>
          <Row xs={1} md={5} className="g-4">
            {Array.from(recHTL).map((item, idx) => (
              <Col key={idx}>
                <Card>
                  <Card.Img src={`./img2.jpg`}></Card.Img>
                  <Card.Body>

                    <Card.Title>{item?.hTName}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{item?.location}</Card.Subtitle>
                    <ListGroup>
                      <ListGroup.Item>
                        Length: {item?.length?.value}{item?.length?.unit}
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Elevation Gain: {item?.elevationGain?.value}{item?.elevationGain?.unit}
                      </ListGroup.Item>
                    </ListGroup>
                  </Card.Body>
                  <Button onClick={() => { navigate(`/hTLocations/${item?._id}`) }}>View Details</Button>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Recommendation;
