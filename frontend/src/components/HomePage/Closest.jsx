import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Row, Col, Container, Button, ListGroup } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

const Closest = () => {
    const nOfItems = 5;
    let [closest, setClosest] = useState([]);
    let navigate = useNavigate();

    let getClosest = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/hTLocations?type=sort&sort=closest&latitude=${localStorage.getItem(
                    "latitude")}&longitude=${localStorage.getItem("longitude")}&page=${nOfItems}`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
            console.log(result);
            setClosest(result.data.result);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        getClosest();
        console.log(Number.parseInt("4.8998"))
    }, []);
    return (
        <div>
            {/* {closest.map((item, i) => {
                return (
                    <div key={i}>
                        <h3>{item.hTName}</h3>
                        <p>{item.location}</p>
                        <p>Approximate Distance from you: {Math.round(Number.parseFloat(item.dist))} km</p>
                        <p>Length: {item.length.value} {item.length.unit}</p>
                    </div>
                )
            })} */}
            <Container style={{padding: "20px"}}>
            <h2>Closest to You</h2>

                <Row xs={1} md={5} className="g-4">
                    {Array.from(closest).map((item, idx) => (
                        <Col key={idx}>
                            <Card>
                                <Card.Img src="/Image-min.jpg"></Card.Img>
                                <Card.Body>

                                    <Card.Title>{item?.hTName}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{item?.location}</Card.Subtitle>
                                    <ListGroup>
                                        {console.log(item?.dist)}
                                        <ListGroup.Item>Approximate Distance from you: {Math.round(Number.parseFloat(item?.dist))} km</ListGroup.Item>
                                        <ListGroup.Item>
                                            Length: {item?.length?.value}{item?.length?.unit}
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Elevation Gain: {item?.elevationGain?.value}{item?.elevationGain?.unit}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                                <Button onClick={() => { navigate(`/hTLocations/${item?._id}`) }}>View Details</Button>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
};

export default Closest;
