import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Card, Container, ListGroup } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import "../styles/button.css";

const SimilarLocations = ({ hTLocId, setHTLocation }) => {
    // console.log(hTLocId);
    let [locations, setLocations] = useState([]);
    let navigate = useNavigate();
    
    let getRecommendation = async () => {
        try {
            let result = await axios({
                url: `http://localhost:8000/hTLocations/recommendation/content/${hTLocId}`,
                method: `get`,
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
            console.log(result.data.result);
            setLocations(result.data.result);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        getRecommendation();
    }, [navigate])
    return (
        <Container>
            <h2 style={{marginTop:"40px", textAlign:"center"}}>Similar Locations</h2>
            {/* <p>{locations[0]?.hTName}</p> */}
            {locations.map((item, i) => {
                return (<Container style={{display: "flex", alignItems:"center", justifyContent: "center"}}>
                    <Card style={{ width: '18rem', marginBottom:"5px" }}>
                        <Card.Img src="/Image-min.jpg"></Card.Img>
                        <Card.Body>
                            <Card.Title>{item.hTLocation.hTName}</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">{item.hTLocation.location}</Card.Subtitle>
                            <ListGroup>
                                <ListGroup.Item>
                                    Length: {item.hTLocation.length?.value}{item.hTLocation.length?.unit}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    Elevation Gain: {item.hTLocation.elevationGain.value}{item.hTLocation.elevationGain.unit}
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                         <button className="detailButton" onClick={async (e) => {
                        try {
                            let result = await axios({
                                url: `http://localhost:8000/hTLocations/${item.hTLocation._id}`,
                                method: `get`,
                                headers: {
                                    Authorization: `Bearer ${localStorage.getItem("token")}`
                                }
                            });
                            // console.log(result);
                            setHTLocation(result.data.result);
                            navigate(`/hTLocations/${item.hTLocation._id}`);
                        }
                        catch (error)
                        {
                            console.log(error);
                        }
                        // navigate(`${location.hTLocation._id}`);
                    }}>view</button>
                    </Card>
                </Container>)
                // return (<div key={i} style={{backgroundColor:"turquoise"}}>
                //     <h3>{location?.hTLocation.hTName}</h3>
                //     <p>Location: {location?.hTLocation.location}</p>
                //     <p>Length: {location.hTLocation.length?.value}{location.hTLocation.length?.unit}</p>
                //     <p>Elevation Gain: {location.hTLocation.elevationGain?.value}{location.hTLocation.elevationGain?.unit}</p>
                //     <p style={{fontSize: "20px", fontWeight:"bold"}}>Match: {Math.round(location.match*100)}%</p>
                //     <button onClick={async (e) => {
                //         try {
                //             let result = await axios({
                //                 url: `http://localhost:8000/hTLocations/${location.hTLocation._id}`,
                //                 method: `get`,
                //                 headers: {
                //                     Authorization: `Bearer ${localStorage.getItem("token")}`
                //                 }
                //             });
                //             // console.log(result);
                //             setHTLocation(result.data.result);
                //             navigate(`/hTLocations/${location.hTLocation._id}`);
                //         }
                //         catch (error)
                //         {
                //             console.log(error);
                //         }
                //         // navigate(`${location.hTLocation._id}`);


                //     }}>view</button>
                // </div>)
            })}
        </Container>
    )
}

export default SimilarLocations