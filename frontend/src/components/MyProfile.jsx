import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import HTLocationComponent from "./HTLocationComponent";
import ShowWishlist from "./ShowWishlist";
import { ToastContainer } from "react-toastify";
import { Card, Container } from "react-bootstrap";
import "../styles/button.css";

const MyProfile = () => {
  let navigate = useNavigate();
  let [user, setUser] = useState("");
  let [wishList, setWishList] = useState([]);
  let getMyProfile = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/users/my-profile`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setUser(result.data.result);
      // console.log(result.data.result);
    } catch (error) {
      console.log(error);
    }
  };
  let getWishList = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/wishLists`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result.data.result);
      result = result.data.result;
      setWishList(result);
      // let hTLocations = [];
      // for (let i = 0; i < result.length; i++) {
      //   let out = await axios({
      //     url: `http://localhost:8000/hTLocations/${result[i].hTLocationId}`,
      //     method: `get`,
      //     headers: {
      //       Authorization: `Bearer ${localStorage.getItem("token")}`,
      //     },
      //   });
      //   hTLocations[i] = out.data.result;
      // }
      // console.log(hTLocations);
      // setWishList(hTLocations);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getMyProfile();
    getWishList();
  }, []);
  return (
    <div style={{ backgroundColor: "whitesmoke" }}>
      <ToastContainer></ToastContainer>
      <Container>
        <Card style={{ backgroundColor: "whitesmoke", border: "none" }}>
          <Card.Body>
            <Card.Title as="h2">My Profile</Card.Title>
            <Card.Text style={{ fontSize: "1.2em" }}>
              <div><b>Name:</b> {user.firstName} {user.lastName}{" "}</div>
              <div><b>Email:</b> {user.email}</div>
              <div><b>Date of Birth:</b> {user.dob?.split("T")[0]}</div>
              <div><b>Address:</b> {user.address}</div>
              <div><b>Gender:</b> {user.gender}</div>
              <div><b>Role:</b> {user.role}</div>
            </Card.Text>
            <div>
              <button
                onClick={() => {
                  navigate("update");
                }}
                className="updateButton"
              >
                Edit
              </button>
            </div>
          </Card.Body>
        </Card>

      </Container>
    </div>


  );
};

export default MyProfile;
