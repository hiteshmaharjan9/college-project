import { Schema } from "mongoose";


let preferenceSchema = new Schema({
    length: {
        type: String,
        required: [true, "length is required"]
    },
    
    elevationGain: {
        type: String,
        required: [true, "elevationGain is required"]
    },
    difficulty: {
        type: String,
        required: [true, "difficulty is required"]
    },
    routeType: {
        type: String,
        required: [true, "routeType is required"]
    }, 
    attributes: {
        type: [String],
        required: [true, "attributes are required"]
    },
    userId: {
        type: Schema.ObjectId,
        required: [true, "userId is required"],
        unique: true
    }
}, {timestamps: true});

export default preferenceSchema;