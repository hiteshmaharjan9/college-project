import { Preference } from "../schema/model.js";



export let createPreference = async(req, res) => {
    try {
        // console.log(req.body);
        let preference = {
            ...req.body,
            userId: req._id
        };
        let result = await Preference.create(preference);
        res.status(201).json({
            success: true,
            message: "preference created successfully",
            result: result
        });
    }
    catch (err)
    {
        res.status(400).json({
            success: false,
            message: err.message
        })
    }
};


export let getPreference = async(req, res) => {
    try {
        let result = await Preference.findOne({userId: req._id});
        if (!result) throw new Error("No preference object available");
        res.status(201).json({
            success: true,
            message: "preference get successful",
            result: result
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        })
    }
}