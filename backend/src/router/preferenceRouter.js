import { Router } from "express";
import { createPreference, getPreference } from "../controller/preferenceController.js";
import isValidToken from "../middleware/isValidToken.js";
import isAuthenticated from "../middleware/isAuthenticated.js";

let preferenceRouter = new Router();

preferenceRouter
  .route("/")
  .post(isValidToken("login"), isAuthenticated, createPreference)
  .get(isValidToken("login"), isAuthenticated, getPreference);

export default preferenceRouter;
