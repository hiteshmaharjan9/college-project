import { Router } from "express";
import { createHTLocationRating, deleteRating, getHTLocationAllRating, getHTLocationRating, getUserAllRating, updateRating } from "../controller/ratingController.js";
import isAuthenticated from "../middleware/isAuthenticated.js";
import isValidToken from "../middleware/isValidToken.js";



let ratingRouter = new Router();

ratingRouter
.route("/users")
.get(isValidToken("login"), isAuthenticated, getUserAllRating)

ratingRouter
.route("/:hTLocId")
.get(getHTLocationAllRating);
// .patch(isValidToken("login"), isAuthenticated, updateRating)
// .delete(isValidToken("login"), isAuthenticated, deleteRating);

ratingRouter
.route("/hTLocations/:hTLocId")
.get(isValidToken("login"), isAuthenticated, getHTLocationRating)
// .get(getHTLocationAllRating)
.post(isValidToken("login"), isAuthenticated, createHTLocationRating)
.patch(isValidToken("login"), isAuthenticated, updateRating)
.delete(isValidToken("login"), isAuthenticated, deleteRating);


export default ratingRouter;