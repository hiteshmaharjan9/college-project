import jwt from "jsonwebtoken";
import { secret_key } from "../config/config.js";

let isAuthenticated = async (req, res, next) => {
  try {
    let bearerToken = req.headers.authorization;
    let token = bearerToken.split(" ")[1];
    let infoObj = jwt.verify(token, secret_key);  
    req._id = infoObj._id;
    next();
  } catch (error) {
    res.status(401).json({
      success: false,  
      message: error.message,
    });
  }
};

export default isAuthenticated;
