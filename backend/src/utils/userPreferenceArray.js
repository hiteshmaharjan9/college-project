function userPreferenceArray(userPreference, itemMatrix)
{
    let userProfileArray = [];

    //length
    if (userPreference.length === "lte20") {
      userProfileArray[0] = 1;
      userProfileArray[1] = 0;
      userProfileArray[2] = 0;
    } else if (userPreference.length === "lte50") {
      userProfileArray[0] = 0;
      userProfileArray[1] = 1;
      userProfileArray[2] = 0;
    } else {
      userProfileArray[0] = 0;
      userProfileArray[1] = 0;
      userProfileArray[2] = 1;
    }

    //elevationGain
    if (userPreference.elevationGain === "lte500") {
      userProfileArray[3] = 1;
      userProfileArray[4] = 0;
      userProfileArray[5] = 0;
    } else if (userPreference.elevationGain === "lte2000") {
      userProfileArray[3] = 0;
      userProfileArray[4] = 1;
      userProfileArray[5] = 0;
    } else {
      userProfileArray[3] = 0;
      userProfileArray[4] = 0;
      userProfileArray[5] = 1;
    }

    //difficulty
    if (userPreference.difficulty === "easy") {
      userProfileArray[6] = 1;
      userProfileArray[7] = 0;
      userProfileArray[8] = 0;
    } else if (userPreference.difficulty === "moderate") {
      userProfileArray[6] = 0;
      userProfileArray[7] = 1;
      userProfileArray[8] = 0;
    } else {
      userProfileArray[6] = 0;
      userProfileArray[7] = 0;
      userProfileArray[8] = 1;
    }

    //routeType
    if (userPreference.routeType === "Loop") {
      userProfileArray[9] = 1;
      userProfileArray[10] = 0;
      userProfileArray[11] = 0;
    } else if (userPreference.routeType === "Out & Back") {
      userProfileArray[9] = 0;
      userProfileArray[10] = 1;
      userProfileArray[11] = 0;
    } else {
      userProfileArray[9] = 0;
      userProfileArray[10] = 0;
      userProfileArray[11] = 1;
    }

    //attributes
    for (let i = 12; i < itemMatrix[0].length; i++) {
      if (userPreference.attributes.includes(itemMatrix[0][i])) {
        console.log(itemMatrix[0][i]);
        userProfileArray[i] = 1;
      } else userProfileArray[i] = 0;
    }
    // console.log(userProfileArray);
    return userProfileArray;

}

export default userPreferenceArray;